# Python tuto
## List of idea to developp
* SVG generator for iObeya calendar
* Python iObeya data analysis
## SVG generator for Calendar
### Main concept & Use case
Create a python code to draw an SVG chart to display Weeks number for a given period.  
Purpose = generate a SVG image which is imported into an iObeya board as a **"Sync image"**.
### Design
The chart shall have two row :
* first row is for Month
* second row is for associated week number
The chart shall have various columns : columns number is equal to the equivalent weeks for three months

**Note** : the columns number could be set as a configuration parameter in the python script. For instance for people who desire four months or two months.
### Scripts algorithm & idea
* a popup to enter the current date
* (option) a set of popup for script parameter (row height, number of columns, etc.)

**Algo** (ideas)
1. Popup for algo parameters
1. Compute the dates and associated weeks
1. group weeks by months
1. draw the SVG rectangle to mimic the chart
1. write weeks and months into SVG
1. export svg
