var svgStroke = document.querySelector('#cercleAModifier')
var p = document.querySelector('.paragraph')
// window.setInterval(function () {
//     p.classList.toggle('blue')
// }, 1000)
var rougit = function () {
    p.classList.toggle('red')
}
p.addEventListener('click', rougit)

// Exemple de script où l'appui souris va venir masqué un élément de la page
var page = document.querySelector('body')
page.addEventListener('click', function (e) {
    var paragraphAffichage = document.querySelector('#aAfficher')
    paragraphAffichage.hidden = true

    //On vient changer aussi l'attribut Style de notre cercle, mais pas au niveau CSS.
    // svgStroke.style.strokeWidth = "40px"
    var svgCSS = document.querySelectorAll(".circleAnimationClass")
    for (index = 0; index < svgCSS.length; index++) {
        svgCSS[index].style.strokeWidth = "40px"

    }

    var svgCSS = document.querySelectorAll("circle")
    for (index = 0; index < svgCSS.length; index++) {
        svgCSS[index].style.strokeWidth = "20px"

    }

})



// Fonction pour venir modifier l'épaisseur de la ligne en fonction du scroll de la souris
window.addEventListener('scroll', function (e) {
    // var svgStroke = document.querySelector('#cercleAModifier')
    svgStroke.style.strokeWidth = window.scrollY * 10

})

/**
 * Fonction pour générer un effet de FADE out sur un ensemble d'élément
 * @param {HTMLElement} element - Element à fournir en input
 */
function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1) {
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

/**
 * Fonction pour l'animation de LOADING PAGE
 */
window.addEventListener("load", function (e) {
    this.setTimeout(function () {

        var loader = document.querySelector('.loader-wrapper')
        fade(loader)

    }, 5000)
    console.log("Hello florent")
})





